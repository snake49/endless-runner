﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveManager : MonoBehaviour
{

    [HideInInspector]
    public SaveData Save { get; private set; }
    [SerializeField]
    private List<Skin> availibleSkins = new List<Skin>();

    private GameManager gameManager;
    private UIManager uiManager;

    private void Awake()
    {
        Save = SaveData.LoadFile();

        if (Save == null)
            Save = new SaveData(availibleSkins.Count);
    }
    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        uiManager = FindObjectOfType<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateSkins(SkinList skinList)
    {
        skinList.UpdateSkins(Save, availibleSkins);
    }

    /// <summary>
    /// Returns the last selected skin
    /// </summary>
    public Skin GetSelectedSkin()
    {
        return availibleSkins[Save.selectedSkin];
    }

    /// <summary>
    /// Puts current score in high score table
    /// </summary>
    public void UpdateHighscore()
    {
        for (int i = 0; i < Save.highScores.Length; i++)
        {
            if (gameManager.score > Save.highScores[i])
            {
                MoveHighScore(Save.highScores[i], Save.highScoreName[i], i + 1);
                Save.highScores[i] = gameManager.score;
                Save.highScoreName[i] = uiManager.GetNameInput();
                break;
            }
        }

        SaveData.SaveFile(Save);
        uiManager.ActiveGameOver();
    }

    public void MoveHighScore(int newScore, string newName, int newIndex)
    {
        if (newIndex < 10)
        {
            //replace score with higher value and recursivly move down list
            MoveHighScore(Save.highScores[newIndex], Save.highScoreName[newIndex], newIndex + 1);
            Save.highScores[newIndex] = newScore;
            Save.highScoreName[newIndex] = newName;
        }
    }

}

[System.Serializable]
public class SaveData
{
    public string[] highScoreName;
    public int[] highScores;
    public int gems;
    public bool[] skinUnlocked;
    public int selectedSkin;

    public SaveData(int availibleSkins)
    {
        highScoreName = new string[] { "", "", "", "", "", "", "", "", "", "" };
        highScores = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        gems = 0;
        skinUnlocked = new bool[availibleSkins];
        skinUnlocked[0] = true;

        for (int i = 1; i < availibleSkins; i++)
        {
            skinUnlocked[i] = false;
        }



    }

    public static void SaveFile(SaveData newSave)
    {
        //create save path
        string saveDestination = Application.persistentDataPath + "/save.dat";
        FileStream stream;

        //get access to save file
        if (File.Exists(saveDestination))
            stream = File.OpenWrite(saveDestination);
        else
            stream = File.Create(saveDestination);

        // save given data
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, newSave);
        stream.Close();


    }

    public static SaveData LoadFile()
    {
        string saveDestination = Application.persistentDataPath + "/save.dat";
        FileStream stream;

        //get access to save file
        if (File.Exists(saveDestination))
            stream = File.OpenRead(saveDestination);
        else
            return null;

        //read save data
        BinaryFormatter formatter = new BinaryFormatter();
        SaveData savedData = formatter.Deserialize(stream) as SaveData;
        stream.Close();

        return savedData;
    }
}
