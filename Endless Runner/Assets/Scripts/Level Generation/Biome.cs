﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Biome", menuName = "Biome")]
public class Biome : ScriptableObject
{
    public List<LevelChunk> biomeChunks;
    [Tooltip("Should start at middle height")]
    public LevelChunk entryChunk;
    [Tooltip("Should end at middle height")]
    public LevelChunk exitChunk;
}
