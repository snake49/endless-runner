﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [Header("Prefabs")]
    [SerializeField]
    private List<Biome> biomes;
    private Biome activeBiome;
    [SerializeField]
    private LevelChunk startChunkPrefab;

    [Header("In Scene")]
    [SerializeField]
    private LevelChunk startingChunk;
    [SerializeField]
    private float generationLength;
    [SerializeField]
    private float despawnLength;
    [SerializeField]
    private float chunkGap = 2;

    private PlayerController player;

    private bool pendingBiomeChange;
    private List<LevelChunk> activeChunks = new List<LevelChunk>();
    private List<LevelChunk> inActiveChunks = new List<LevelChunk>();
    private Vector3 startChunkPos;

    // Start is called before the first frame update
    void Start()
    {
        if (biomes.Count > 0)
        {
            activeBiome = biomes[0];
        }
        player = FindObjectOfType<PlayerController>();
        activeChunks.Add(startingChunk);
        startChunkPos = startingChunk.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //deactive any chunks behind the player
        if(player.transform.position.x - activeChunks[0].endPos.x >= despawnLength)
        {
            activeChunks[0].gameObject.SetActive(false);
            inActiveChunks.Add(activeChunks[0]);
            activeChunks.Remove(activeChunks[0]);
        }

        LevelChunk furthestChunk = activeChunks[activeChunks.Count - 1];

        //create new chunks as the player approaches
        if(furthestChunk.startPos.x  - player.transform.position.x <= generationLength)
        {
            GenerateNewChunk(furthestChunk);
        }
        
    }

    protected void GenerateNewChunk(LevelChunk lastChunk)
    {
        LevelChunk newChunk = null;
        bool chunkValid = false;

        //biome remains the same
        if (pendingBiomeChange == false)
        {
            //Find new chunk that can be reached from the previous one
            while (!chunkValid)
            {
                newChunk = activeBiome.biomeChunks[Random.Range(0, activeBiome.biomeChunks.Count)];

                switch (lastChunk.endHeight)
                {
                    case LevelChunk.Height.High:
                        if (newChunk.startHeight != LevelChunk.Height.Low)
                            chunkValid = true;
                        break;
                    case LevelChunk.Height.Middle:
                        if (newChunk.startHeight != LevelChunk.Height.High)
                            chunkValid = true;
                        break;
                    case LevelChunk.Height.Low:
                        if (newChunk.startHeight == LevelChunk.Height.Low)
                            chunkValid = true;
                        break;
                }

                //check same chunk isn't being placed twice in a row
                if (newChunk.gameObject == lastChunk.prefabLink)
                    chunkValid = false;
            }

            // Create new chunk
            LevelChunk instantiatedChunk = GetChunk(newChunk, lastChunk.transform.position + Vector3.right * (lastChunk.length / 2 + newChunk.length / 2 + chunkGap));
            activeChunks.Add(instantiatedChunk.GetComponent<LevelChunk>());
        }
        //biome is being changed
        else
        {
            // check if height to change biome is correct
            if (lastChunk.endHeight == LevelChunk.Height.Middle || lastChunk.endHeight == LevelChunk.Height.High)
            {
                // get new biome
                Biome newBiome = biomes[Random.Range(0, biomes.Count)];

                while (newBiome == activeBiome)
                {
                    newBiome = biomes[Random.Range(0, biomes.Count)];
                }

                LevelChunk exitChunk = GetChunk(activeBiome.exitChunk, lastChunk.transform.position + Vector3.right * (lastChunk.length / 2 + activeBiome.exitChunk.length / 2 + chunkGap));
                activeChunks.Add(exitChunk.GetComponent<LevelChunk>());
                LevelChunk entryChunk = GetChunk(newBiome.entryChunk, exitChunk.transform.position + Vector3.right * (exitChunk.length / 2 + newBiome.entryChunk.length / 2));
                activeChunks.Add(entryChunk.GetComponent<LevelChunk>());

                activeBiome = newBiome;
                pendingBiomeChange = false;
            }
            // Cannot change biome yet
            else
            {
                // Find new chunk that can be reached from the previous one
                while (!chunkValid)
                {
                    newChunk = activeBiome.biomeChunks[Random.Range(0, activeBiome.biomeChunks.Count)];

                    switch (lastChunk.endHeight)
                    {
                        case LevelChunk.Height.High:
                            if (newChunk.startHeight != LevelChunk.Height.Low)
                                chunkValid = true;
                            break;
                        case LevelChunk.Height.Middle:
                            if (newChunk.startHeight != LevelChunk.Height.High)
                                chunkValid = true;
                            break;
                        case LevelChunk.Height.Low:
                            if (newChunk.startHeight == LevelChunk.Height.Low)
                                chunkValid = true;
                            break;
                    }

                    // Make sure it can be reach a biome change
                    if (newChunk.endHeight == LevelChunk.Height.Low)
                        chunkValid = false;
                }

                // Create new chunk
                LevelChunk instantiatedChunk = GetChunk(newChunk, lastChunk.transform.position + Vector3.right * (lastChunk.length / 2 + newChunk.length / 2 + chunkGap));
                activeChunks.Add(instantiatedChunk.GetComponent<LevelChunk>());
            }
        }
    }

    public void Regenerate()
    {
        //Destroy old level
        foreach(LevelChunk chunk in activeChunks)
        {
            chunk.gameObject.SetActive(false);
            inActiveChunks.Add(chunk);
        }

        activeChunks.Clear();
        activeBiome = biomes[0];
        pendingBiomeChange = false;

        LevelChunk newStart = GetChunk(startChunkPrefab, startChunkPos);
        activeChunks.Add(newStart);
    }

    public LevelChunk GetChunk(LevelChunk chunkType, Vector3 position)
    {
        LevelChunk returningChunk = null;

        //find existing copy of desired level chunk
        foreach (LevelChunk chunk in inActiveChunks)
        {
            if(chunk.prefabLink == chunkType.gameObject)
            {
                returningChunk = chunk;
                break;
            }
        }

        if (returningChunk != null)
        {
            inActiveChunks.Remove(returningChunk);
            returningChunk.gameObject.SetActive(true);
            returningChunk.transform.position = position;
        }
        //create a new copy if none can be found
        else
        {
            returningChunk = Instantiate(chunkType, position, Quaternion.identity);
            returningChunk.prefabLink = chunkType.gameObject;
        }

        return returningChunk;
    }

    public void PauseChunks(bool isPaused)
    {
        foreach(LevelChunk chunk in activeChunks)
        {
            foreach (Interactable obj in chunk.interactables)
            {
                Animator animator = obj.GetComponent<Animator>();
                if(animator != null)
                {
                    animator.enabled = !isPaused;
                }
            }
        }
    }

    public void ChangeBiome()
    {
        if (biomes.Count > 1)
            pendingBiomeChange = true;
        else
            Debug.LogWarning("Could not change biome as only one is available");
    }

    /// <summary>
    /// Moves all active chunks forward by a given amount
    /// </summary>
    /// <param name="distance"></param>
    public void ShiftChunks(float distance)
    {
        foreach(LevelChunk chunk in activeChunks)
        {
            chunk.transform.position += new Vector3(distance, 0, 0);
        }
    }
}
