﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChunk : MonoBehaviour
{
    public enum Height
    {
        Low,
        Middle,
        High
    }

    public Height startHeight;
    public Height endHeight;

    [HideInInspector]
    public Vector3 endPos { get; private set; }
    [HideInInspector]
    public Vector3 startPos { get; private set; }

    [HideInInspector]
    public List<Interactable> interactables = new List<Interactable>();

    [Tooltip("Measured in tiles")]
    public float length;

    [HideInInspector]
    //the prefab this LevelChunk is a clone of
    public GameObject prefabLink;

    private void OnEnable()
    {
        UpdatePositions();

        foreach(Interactable obj in interactables)
        {
            obj.gameObject.SetActive(true);
        }
    }

    private void Start()
    {
        interactables.AddRange(GetComponentsInChildren<Interactable>(true));
    }

    private void Update()
    {
        UpdatePositions();
    }

    private void OnDrawGizmos()
    {

        UpdatePositions();

        //draw level at which chunk ends
        Gizmos.DrawSphere(endPos, 0.3f);

        //draw level at which chunk starts
        Gizmos.DrawSphere(startPos, 0.3f);
    }

    /// <summary>
    /// Updates the end and start positions of chunks
    /// </summary>
    protected void UpdatePositions()
    {
        endPos = transform.position + Vector3.right * (length / 2);

        switch (endHeight)
        {
            case Height.Low:
                endPos += Vector3.down * 3;
                break;
            case Height.Middle:
                break;
            case Height.High:
                endPos += Vector3.up * 3;
                break;
        }

        startPos = transform.position + Vector3.left * (length / 2);

        switch (startHeight)
        {
            case Height.Low:
                startPos += Vector3.down * 3;
                break;
            case Height.Middle:
                break;
            case Height.High:
                startPos += Vector3.up * 3;
                break;
        }
    }
}
