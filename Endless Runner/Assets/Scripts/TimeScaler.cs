﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaler : MonoBehaviour
{
    [Range(0, 5)]
    public float timeScale = 1;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = timeScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale != timeScale)
            Time.timeScale = timeScale;
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F2))
        {
            timeScale = 1;
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            timeScale = 0.5f;
        }

        if (Input.GetKeyDown(KeyCode.F4))
        {
            timeScale = 0.25f;
        }

        if (Input.GetKeyDown(KeyCode.F5))
        {
            timeScale = 0.1f;
        }
#endif
    }

     
}
