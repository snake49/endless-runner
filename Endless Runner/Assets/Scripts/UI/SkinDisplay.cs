﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SkinDisplay : MonoBehaviour
{
    [SerializeField]
    protected TMP_Text skinName;
    [SerializeField]
    protected TMP_Text skinCost;
    [SerializeField]
    protected Image skinDisplay;
    [SerializeField]
    protected Button buyButton;
    [SerializeField]
    protected TMP_Text buyText;
    [SerializeField]
    protected Skin skin;

    //Location of the skin in save
    protected int skinLocation;

    private SaveManager saveManager;
    public SaveManager SaveManager
    {
        get { return saveManager; }
        set { if (saveManager == null) saveManager = value; }
    }

    private GameManager gameManager;
    public GameManager GameManager
    {
        get { return gameManager; }
        set { if (gameManager == null) gameManager = value; }
    }

    private void Update()
    {
        UpdateButton(saveManager.Save.skinUnlocked[skinLocation] == true);
    }
    
    /// <summary>
    /// Apply given skin to this display
    /// </summary>
    /// <param name="newSkin">Skin to be applied</param>
    /// <param name="saveLocation">Location of the skin in save</param>
    public void ApplySkin(Skin newSkin, int saveLocation)
    {
        skinName.text = newSkin.name;
        skinCost.text = newSkin.unlockCost.ToString();
        skinDisplay.sprite = newSkin.baseSprite;
        skin = newSkin;
        skinLocation = saveLocation;
        UpdateButton(saveManager.Save.skinUnlocked[saveLocation]);
    }

    public void UpdateButton(bool hasBought)
    {
        //update button text
        if(hasBought)
        {
            if (saveManager.Save.selectedSkin == skinLocation)
                buyText.text = "Selected";
            else
                buyText.text = "Select";
        }
        else
        {
            buyText.text = "Buy";
        }
    }

    public void ActivateSkin()
    {
        //if skin is unlock, equip skin
        if(saveManager.Save.skinUnlocked[skinLocation] == true)
        {
            saveManager.Save.selectedSkin = skinLocation;
            gameManager.currentPlayer.ApplySkin(skin);
        }
        //else attempt to buy and equip it if successful
        else
        {
            if(saveManager.Save.gems >= skin.unlockCost)
            {
                saveManager.Save.skinUnlocked[skinLocation] = true;
                saveManager.Save.selectedSkin = skinLocation;
                gameManager.currentPlayer.ApplySkin(skin);
                saveManager.Save.gems -= skin.unlockCost;
                UpdateButton(true);
                SaveData.SaveFile(saveManager.Save);
            }
        }
    }
}
