﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinList : MonoBehaviour
{
    [SerializeField]
    protected SkinDisplay skinDisplayer;

    protected List<SkinDisplay> skinList = new List<SkinDisplay>();
    protected GameManager gameManager;
    protected SaveManager saveManager;

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        saveManager = FindObjectOfType<SaveManager>();
    }

    /// <summary>
    /// Links the displayed skins with saved data
    /// </summary>
    /// <param name="saveData"></param>
    /// <param name="skins"></param>
    public void UpdateSkins(SaveData saveData, List<Skin> skins)
    {
        for (int i = 0; i < skins.Count; i++)
        {
            //create new skinDisplay if there is not enough
            if (skinList.Count <= i)
            {
                skinList.Add(Instantiate(skinDisplayer, transform));
                skinList[i].GameManager = gameManager;
                skinList[i].SaveManager = saveManager;
            }

            skinList[i].ApplySkin(skins[i], i);
        }
    }
}
