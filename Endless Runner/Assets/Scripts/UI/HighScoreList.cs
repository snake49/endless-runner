﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScoreList : MonoBehaviour
{
    [SerializeField]
    private List<HighScoreDisplay> highScoreDisplay = new List<HighScoreDisplay>();

    public void UpdateHighScores(SaveData saveData)
    {
        for(int i = 0; i < saveData.highScores.Length; i++)
        {
            highScoreDisplay[i].place.text = (i + 1).ToString() + ".";
            highScoreDisplay[i].playerName.text = saveData.highScoreName[i];
            highScoreDisplay[i].score.text = saveData.highScores[i].ToString();
        }
    }
}
