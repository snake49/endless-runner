﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GameManager : MonoBehaviour
{
    public int score { get; private set; }
    [SerializeField]
    private float biomeLength = 100;
    private float biomeDistance = 0;
    [SerializeField]
    [Tooltip("Distance after which all object will be moved back towards the origin")]
    private float shiftLength = 1000;
    private float timesShifted = 0;
    [SerializeField]
    private GameObject sign;

    private float bonusScore;
    [HideInInspector]
    public PlayerController currentPlayer;

    protected CameraFollow currentCamera;
    protected LevelGenerator levelGenerator;
    protected List<ParallaxBackground> backgrounds = new List<ParallaxBackground>();
    protected SaveManager saveManager;
    protected UIManager uiManager;

    protected float distance;
    protected Vector3 playerStartPos;
    protected Vector3 cameraStartPos;


    // Start is called before the first frame update
    void Start()
    {
        saveManager = FindObjectOfType<SaveManager>();
        uiManager = FindObjectOfType<UIManager>();

        levelGenerator = FindObjectOfType<LevelGenerator>();
        currentPlayer = FindObjectOfType<PlayerController>();
        currentCamera = FindObjectOfType<CameraFollow>();
        backgrounds.AddRange(FindObjectsOfType<ParallaxBackground>());

        cameraStartPos = currentCamera.transform.position;
        playerStartPos = currentPlayer.transform.position;

        currentPlayer.ApplySkin(saveManager.GetSelectedSkin());

        biomeDistance = biomeLength;
    }

    // Update is called once per frame
    void Update()
    {
        distance = currentPlayer.transform.position.x - playerStartPos.x ;
        score = (int)(distance + bonusScore + (shiftLength * timesShifted));

        if(distance + (shiftLength * timesShifted) >= biomeDistance)
        {
            levelGenerator.ChangeBiome();
            biomeDistance += biomeLength;
        }

        if(distance >= shiftLength)
        {
            sign.SetActive(false);
            timesShifted += 1;
            currentPlayer.transform.position -= new Vector3(shiftLength, 0, 0);
            currentCamera.transform.position -= new Vector3(shiftLength, 0, 0);
            levelGenerator.ShiftChunks(-shiftLength);
            
            foreach(ParallaxBackground background in backgrounds)
            {
                background.ShiftBackground(-shiftLength);
            }
        }

#if UNITY_EDITOR
        //editor debug
        if (Input.GetKeyDown(KeyCode.P))
        {
            ResetGame(true);
        }
#endif
    }

    public void ResetGame(bool startRunning)
    {
        //reset ui
        uiManager.ActivateInGameUI();
        ChangePauseState(false);
        uiManager.UpdateGems();
        //Reset Player and score
        currentPlayer.transform.position = playerStartPos;

        if(startRunning)
            currentPlayer.Respawn();
        else
            currentPlayer.MenuDisplay();

        bonusScore = 0;
        distance = 0;
        timesShifted = 0;
        biomeDistance = biomeLength;

        //Reset Camera
        currentCamera.transform.position = cameraStartPos;

        //Reset level
        levelGenerator.Regenerate();
        sign.SetActive(true);
    }

    public void PlayerDead()
    {
        uiManager.DeactiveIngameUI();
        currentCamera.ChangeFollow(false);

        StartCoroutine(uiManager.GameOverDelay());
    }

    public void ChangePauseState(bool isPaused)
    {
        currentPlayer.PausePlayer(isPaused);
        levelGenerator.PauseChunks(isPaused);
        uiManager.ChangePauseState(isPaused);
        currentCamera.ChangeFollow(!isPaused);
    }

    public void AddScore(int scoreBonus, int gemBonus = 0)
    {
        bonusScore += scoreBonus;
        saveManager.Save.gems += gemBonus;
        uiManager.UpdateGems();
    }
}
