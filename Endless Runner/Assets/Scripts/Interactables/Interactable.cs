﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class Interactable : MonoBehaviour
{
    public abstract void EnterAny(PlayerController player);

    public virtual void EnterTop(PlayerController player)
    {
    }

    public virtual void EnterBottom(PlayerController player)
    {
    }

    public virtual void EnterSide(PlayerController player)
    {
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //check if collision is with player
        if(collision.tag == "Player")
        {
            // find which collision type occurred
            PlayerController player = collision.GetComponent<PlayerController>();
            float xOffset = player.transform.position.x - transform.position.x;
            float yOffset = player.transform.position.y - transform.position.y;

            EnterAny(player);

            if (Mathf.Abs(xOffset) > Mathf.Abs(yOffset))
                EnterSide(player);
            else
            {
                if (yOffset > 0)
                    EnterTop(player);
                else
                    EnterBottom(player);
            }
        }
    }

    public void DestroySelf()
    {
        gameObject.SetActive(false);
    }
}
