﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : Interactable
{
    public override void EnterAny(PlayerController player)
    {
        player.KillPlayer();
    }
}
