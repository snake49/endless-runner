﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : ScoreBonus
{
    public override void EnterAny(PlayerController player)
    {
        if (!collected)
        {
            manager.AddScore((int)scoreBonus, 1);
            OnPickup();
        }
       
    }
}
