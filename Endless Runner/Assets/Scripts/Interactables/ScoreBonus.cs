﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBonus : Interactable
{
    [SerializeField]
    protected int scoreBonus;

    protected GameManager manager;
    protected Animator animator;

    protected bool collected = false;

    private void OnEnable()
    {
        collected = false;
    }

    private void Start()
    {
        manager = FindObjectOfType<GameManager>();
        animator = GetComponent<Animator>();
    }

    public override void EnterAny(PlayerController player)
    {
        if (!collected)
        {
            manager.AddScore(scoreBonus);
            OnPickup();
        }
    }

    protected void OnPickup()
    {
        animator.SetTrigger("Collected");
        collected = true;
    }
}
