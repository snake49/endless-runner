﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncer : Interactable
{
    [SerializeField]
    protected float bounceStrength = 25;
    protected Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public override void EnterAny(PlayerController player)
    {
    }

    public override void EnterSide(PlayerController player)
    {
        player.KillPlayer();
    }

    public override void EnterTop(PlayerController player)
    {
        animator.SetTrigger("Dead");
        player.Bounce(bounceStrength);
    }
}
