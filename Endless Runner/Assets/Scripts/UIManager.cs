﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [Header("Menu")]
    public GameObject menuUI;

    [Header("In Game")]
    public GameObject inGameUI;
    public TextMeshProUGUI scoreText;
    public TMP_Text gemText;
    [Tooltip("Delay before Game Over UI appears")]
    [SerializeField]
    private float gameOverDelay = 1;

    [Header("Paused")]
    public GameObject PauseUI;

    [Header("High Scores")]
    public GameObject highScoreScreen;
    public GameObject highScoreEnter;
    public TMP_InputField nameInput;
    public HighScoreList highScores;

    [Header("GameOver")]
    public GameObject gameoverUI;
    public TMP_Text finalScoreText;

    [Header("Shop")]
    public GameObject shopUI;
    public SkinList skinList;
    public TMP_Text gemAmountText;

    private GameObject currentUI;
    private GameManager gameManager;
    private SaveManager saveManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        saveManager = FindObjectOfType<SaveManager>();

        //ensure correct ui is active
        menuUI.SetActive(true);
        inGameUI.SetActive(false);
        PauseUI.SetActive(false);
        gameoverUI.SetActive(false);
        highScoreEnter.SetActive(false);
        highScoreScreen.SetActive(false);
        shopUI.SetActive(false);
        currentUI = menuUI;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score: " + gameManager.score;
    }

    public void SwitchToMenu(GameObject menu)
    {
        currentUI.SetActive(false);
        currentUI = menu;
        currentUI.SetActive(true);
    }

    public void GoToShop()
    {
        SwitchToMenu(shopUI);
        saveManager.UpdateSkins(skinList);
        gemAmountText.text = saveManager.Save.gems.ToString();
    }

    public void GoToMainMenu()
    {
        gameManager.ResetGame(false);
        SwitchToMenu(menuUI);
    }

    public void GoToHighScores()
    {
        SwitchToMenu(highScoreScreen);
        highScores.UpdateHighScores(saveManager.Save);
    }

    public void ActivateInGameUI()
    {
        SwitchToMenu(inGameUI);
    }

    public void DeactiveIngameUI()
    {
        inGameUI.SetActive(false);
    }

    public void ChangePauseState(bool isPaused)
    {
        if (isPaused)
            SwitchToMenu(PauseUI);
        else
            SwitchToMenu(inGameUI);
    }

    public string GetNameInput()
    {
        return nameInput.text;
    }

    public void ActiveGameOver()
    {
        SwitchToMenu(gameoverUI);
    }

    public void UpdateGems()
    {
        gemText.text = saveManager.Save.gems.ToString();
        SaveData.SaveFile(saveManager.Save);
    }

    public IEnumerator GameOverDelay()
    {
        yield return new WaitForSecondsRealtime(gameOverDelay);
        finalScoreText.text = "Your score was: " + (int)gameManager.score;

        if (gameManager.score > saveManager.Save.highScores[9])
            SwitchToMenu(highScoreEnter);
        else
            SwitchToMenu(gameoverUI);
    }
}
