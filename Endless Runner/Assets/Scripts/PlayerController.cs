﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    protected float movementSpeed = 5;
    [SerializeField]
    protected float jumpAmount = 5;
    [SerializeField]
    protected float gravity;
    [Tooltip("Distance below the feet the ground will be detected")]
    [SerializeField]
    protected float groundDetectionBonus = 0.05f;
    [Tooltip("Going below this height will kill the player")]
    [SerializeField]
    protected float deathHeight = -2;

    protected Animator animator;
    protected CircleCollider2D footCollider;
    protected BoxCollider2D headCollider;
    protected GameManager manager;
    protected Rigidbody2D rigidBody;
    protected bool paused = false;

    protected bool running = false;
    protected Vector2 movement = Vector2.zero;
    protected float verticalVelocity;
    protected bool grounded = false;
    protected Vector3 startPos;
    protected bool alive = true;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        footCollider = GetComponent<CircleCollider2D>();
        headCollider = GetComponent<BoxCollider2D>();
        rigidBody = GetComponent<Rigidbody2D>();
        manager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (paused != true)
        {
            //movement to be performed this frame
            Vector3 movement = Vector3.zero;

            GroundDetection();
            CeilingCheck();

            //apply gravity
            if (!grounded)
            {
                verticalVelocity -= gravity * Time.deltaTime;
            }
            else
            {
                verticalVelocity = Mathf.Max(0,verticalVelocity);
            }

            //Horizontal movement
            if (running)
            {
                movement += transform.right * movementSpeed;
                animator.SetBool("Running", true);
            }
            else
            {
                animator.SetBool("Running", false);
            }

            //apply vertical velocity to animator and movement
            animator.SetFloat("Vertical Velocity", verticalVelocity);
            movement += transform.up * verticalVelocity;

            if (transform.position.y <= deathHeight && alive)
                KillPlayer();

            //apply movement
            transform.Translate(movement * Time.deltaTime);
        }
    }

    public bool GroundDetection()
    {
        if (!alive)
        {
            grounded = false;
            return false;
        }

        bool lastGrounded = grounded;

        //get position of feet
        float feetHeight = (-footCollider.offset.y + footCollider.radius) * transform.lossyScale.y + groundDetectionBonus;

        //set up hit array and filter
        RaycastHit2D[] hits = new RaycastHit2D[1];
        ContactFilter2D filter = new ContactFilter2D();
        filter.SetLayerMask(LayerMask.GetMask("Default"));

        //raycast to detect ground
        Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.down, filter, hits, feetHeight);

        //is grounded if a collider is detected
        if (hits[0].collider != null)
        {
            animator.SetBool("Grounded", true);
            grounded = true;
        }
        else
        {
            animator.SetBool("Grounded", false);
            grounded = false;
        }

        return grounded;
    }

    public void CeilingCheck()
    {
        if(alive == false)
        {
            return;
        }

        //calcuate height of head
        float headHeight = (headCollider.offset.y + headCollider.size.y) * transform.lossyScale.y + groundDetectionBonus;

        //set up hit array and filter
        RaycastHit2D[] hits = new RaycastHit2D[1];
        ContactFilter2D filter = new ContactFilter2D();
        filter.SetLayerMask(LayerMask.GetMask("Default"));

        //raycast to detect head
        Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.up, filter, hits, headHeight);

        //reset velocity if a collider is detected
        if (hits[0].collider != null)
        {
            verticalVelocity = Mathf.Min(0, verticalVelocity);
        }
    }

    public void KillPlayer()
    {
        running = false;
        alive = false;
        verticalVelocity = jumpAmount;
        animator.SetBool("Alive", false);
        manager.PlayerDead();
        rigidBody.simulated = false;
    }

    /// <summary>
    /// Respawns the player running
    /// </summary>
    public void Respawn()
    {
        running = true;
        alive = true;
        verticalVelocity = 0;
        animator.SetBool("Alive", true);
        animator.SetBool("Running", true);
        rigidBody.simulated = true;
        animator.SetFloat("Vertical Velocity", verticalVelocity);
    }

    /// <summary>
    ///set ups to player to be displayed on the main menu
    /// </summary>
    public void MenuDisplay()
    {
        running = false;
        alive = true;
        verticalVelocity = 0;
        animator.SetBool("Alive", true);
        animator.SetBool("Running", false);
        rigidBody.simulated = true;
        animator.SetFloat("Vertical Velocity", verticalVelocity);
    }

    public void PausePlayer(bool isPaused)
    {
        animator.enabled = !isPaused;
        paused = isPaused; 
    }

    public void Jump()
    {
        if(grounded)
            verticalVelocity += jumpAmount;
    }

    public void ApplySkin(Skin skin)
    {
        //changes characters current sprite and animations
        GetComponent<SpriteRenderer>().sprite = skin.baseSprite;
        animator.runtimeAnimatorController = skin.animations;
    }

    /// <summary>
    /// Changes player vertical velocity
    /// </summary>
    /// <param name="newBounce"></param>
    public void Bounce(float newBounce)
    {
        verticalVelocity = newBounce;
    }

}
