﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    private GameObject cameraFocus;
    [SerializeField]
    private float focusStrength = 5;
    private Vector3 focusOffset;
    private bool following = true;
    [SerializeField]
    private bool followVertical = true;

    // Start is called before the first frame update
    void Start()
    {
        FocusOnTarget();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(following)
        {
            if(followVertical)
                transform.position = Vector3.Lerp(transform.position, cameraFocus.transform.position + focusOffset, Time.deltaTime * focusStrength);
            else
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3((cameraFocus.transform.position + focusOffset).x, transform.position.y, transform.position.z), Time.deltaTime * focusStrength);
            }
        }
    }

    public void FocusOnTarget()
    {
        focusOffset = transform.position - cameraFocus.transform.position;
        following = true;
    }

    public void ChangeFollow(bool newFollow)
    {
        following = newFollow;
    }
}
