﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    [SerializeField]
    private List<Transform> parallexedObjects = new List<Transform>();
    [SerializeField]
    private float maxBack = 100;
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private float limits;
    protected Vector3 lastPos;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = FindObjectOfType<Camera>();
        lastPos = mainCamera.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateParallax();
    }

    public void UpdateParallax()
    {
        foreach (Transform obj in parallexedObjects)
        {
            //move each object to be parallexed backwards
            obj.position -= ((lastPos - mainCamera.transform.position) * obj.position.z / maxBack);

            //if object is behind camera, move to front.
            if (obj.position.x - mainCamera.transform.position.x < limits)
            {
                obj.position = new Vector3(obj.position.x + (obj.GetComponent<SpriteRenderer>().sprite.bounds.size.x * parallexedObjects.Count), obj.position.y, obj.position.z);
            }

            // if is behind camera, move to back
            if (mainCamera.transform.position.x - obj.position.x < limits)
            {
                obj.position = new Vector3(obj.position.x - (obj.GetComponent<SpriteRenderer>().sprite.bounds.size.x * parallexedObjects.Count), obj.position.y, obj.position.z);
            }

        }

        lastPos = mainCamera.transform.position;
    }

    /// <summary>
    /// Moves all parallaxed objects forward by a given amount
    /// </summary>
    /// <param name="distance"></param>
    public void ShiftBackground(float distance)
    {
        foreach(Transform obj in parallexedObjects)
        {
            obj.position += new Vector3(distance, 0, 0);
        }
        lastPos = mainCamera.transform.position;
    }
}
