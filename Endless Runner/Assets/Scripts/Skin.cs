﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Skin", menuName = "Skins/ new Skin")]
public class Skin : ScriptableObject
{
    public Sprite baseSprite;
    public RuntimeAnimatorController animations;
    public int unlockCost;
}
